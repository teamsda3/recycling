package pl.gda11_3.buyrecycle.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;

    private Names name;
    private Forms form;
    private String color;
    private Integer price;
    private String description;
    private int amount;

}
