package pl.gda11_3.buyrecycle.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@Entity

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Offer {

    private Long id;

    private PackageType packageType;
    private String seal;
    private Material material;
    private String localization;
    private String description;

}
