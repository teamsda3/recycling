package pl.gda11_3.buyrecycle.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Controller

public class OfferController {


    @GetMapping(path = "/addOfferForm")
    public String getOfferForm(Model model) {
        Offers offer = new Offers();

        List<Categories> categoriesList = new ArrayList<Categories>(EnumSet.allOf(Categories.class));
        List<RegionsPL> regionsList = new ArrayList<RegionsPL>(EnumSet.allOf(RegionsPL.class));
        model.addAttribute("regionsList", regionsList);
        model.addAttribute("categoriesList", categoriesList);

        model.addAttribute("offer", offer);
        LocalDate defaultDate = LocalDate.now();

        model.addAttribute("defaultDate", defaultDate);

        return "addOfferForm";
    }
}
