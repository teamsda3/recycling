package pl.gda11_3.buyrecycle.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Controller
public class IndexController {

    @GetMapping(path = "/index")
    public String getOfferForm(Model model) {


        return "index";
    }
}
